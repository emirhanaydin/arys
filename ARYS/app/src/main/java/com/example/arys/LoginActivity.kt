package com.example.arys

import android.animation.Animator
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    companion object {
        const val TAG = "LoginActivity"
    }

    private lateinit var auth: FirebaseAuth
    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        buttonLogin.setOnClickListener(onClickButtonLogin)
        textViewRegister.setOnClickListener(onClickTextViewRegister)

        splashCountdown.start()
    }

    override fun onStart() {
        super.onStart()

        auth = FirebaseAuth.getInstance()
    }

    private val splashCountdown = object : CountDownTimer(1500, Long.MAX_VALUE) {
        override fun onFinish() {
            textViewApp.visibility = View.GONE
            progressBarSplash.visibility = View.GONE
            rootView.setBackgroundColor(ContextCompat.getColor(this@LoginActivity, R.color.colorSplashText))
            imageViewIcApp.setImageResource(R.drawable.ic_app)
            startAnimation()
        }

        override fun onTick(millisUntilFinished: Long) {}
    }

    private fun startAnimation() {
        val animator = imageViewIcApp.animate()

        animator.apply {
            x(50f)
            y(100f)
            duration = 1000
            setListener(animatorListener)
        }
    }

    private val animatorListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {}

        override fun onAnimationEnd(animation: Animator?) {
            viewAfterAnim.visibility = View.VISIBLE
        }

        override fun onAnimationCancel(animation: Animator?) {}

        override fun onAnimationStart(animation: Animator?) {}
    }

    private val onClickButtonLogin = View.OnClickListener {
        it.isEnabled = false
        progressBarSplash.visibility = View.VISIBLE

        email = editTextEmail.text.toString()
        val password = editTextPassword.text.toString()

        auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener(onSuccessListenerLogin)
            .addOnSuccessListener(onSuccessListener)
            .addOnFailureListener(onFailureListener)
    }

    private val onClickTextViewRegister = View.OnClickListener {
        val intent = Intent(this, RegisterActivity::class.java)

        startActivityForResult(intent, RequestCodes.SIGN_UP)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data == null) return

        when (requestCode) {
            RequestCodes.SIGN_UP -> {
                if (resultCode == Activity.RESULT_OK) {
                    buttonLogin.isEnabled = false
                    progressBarSplash.visibility = View.VISIBLE

                    email = data.getStringExtra(IntentExtras.EMAIL)
                    val password = data.getStringExtra(IntentExtras.PASSWORD)

                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnSuccessListener(onSuccessListenerRegister)
                        .addOnSuccessListener(onSuccessListener)
                        .addOnFailureListener(onFailureListener)
                }
            }
        }
    }

    private val onSuccessListenerLogin = OnSuccessListener<AuthResult> {
        Log.i(TAG, "signInWithEmailAndPassword:success")
    }

    private val onSuccessListenerRegister = OnSuccessListener<AuthResult> {
        Log.i(TAG, "createUserWithEmailAndPassword:success")
    }

    private val onSuccessListener = OnSuccessListener<AuthResult> {
        val intent = Intent(this, UserActivity::class.java)
        intent.putExtra(IntentExtras.EMAIL, email)
        startActivity(intent)

        finish()
    }

    private val onFailureListener = OnFailureListener {
        Log.e(TAG, "createUserWithEmailAndPassword:failure", it)
        Toast.makeText(baseContext, getString(R.string.authentication_failed), Toast.LENGTH_LONG).show()
    }
}
