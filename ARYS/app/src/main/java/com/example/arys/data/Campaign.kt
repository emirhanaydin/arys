package com.example.arys.data

import com.example.arys.Timestamp
import com.google.type.LatLng

data class Campaign(
    val firmaAdi: String,
    val firmaLokasyon: LatLng,
    val kampanyaIcerik: String,
    val kampanyaSuresi: Long,
    val timestamp: Timestamp
)