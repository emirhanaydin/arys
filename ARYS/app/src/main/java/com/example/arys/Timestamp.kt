package com.example.arys

import com.google.firebase.firestore.FieldValue

data class Timestamp(
    val createdAt: FieldValue = FieldValue.serverTimestamp(),
    val updatedAt: FieldValue
)