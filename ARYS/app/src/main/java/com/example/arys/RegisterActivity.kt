package com.example.arys

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        buttonSignUp.setOnClickListener(onClickButtonSignUp)
    }

    private val onClickButtonSignUp = View.OnClickListener {
        it.isEnabled = false

        intent.putExtra(IntentExtras.EMAIL, editTextEmail.text.toString())
        intent.putExtra(IntentExtras.PASSWORD, editTextPassword.text.toString())

        setResult(Activity.RESULT_OK, intent)

        finish()
    }
}
