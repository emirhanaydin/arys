package com.example.arys.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.example.arys.R
import com.example.arys.data.Type

class TypesAdapter(private val dataSet: List<Type>) : RecyclerView.Adapter<TypesAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val checkBox: CheckBox = view.findViewById(R.id.checkBox)

        fun bind(type: Type) {
            checkBox.isChecked = type.aktif
            checkBox.text = type.ad
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.list_item_type, p0, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val type = dataSet[p1]

        p0.bind(type)
    }
}