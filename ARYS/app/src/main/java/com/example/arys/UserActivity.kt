package com.example.arys

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.SeekBar
import com.example.arys.adapters.TypesAdapter
import com.example.arys.data.Type
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    companion object {
        const val TAG = "UserActivity"
    }

    private val types = mutableListOf<Type>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        val email = intent.getStringExtra(IntentExtras.EMAIL)
        textViewUser.text = email

        textViewProgress.text = getString(R.string.num_meters, seekBar.progress + 10)
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener)

        val db = FirebaseFirestore.getInstance()
        val ref = db.collection("turler")
        ref.get()
            .addOnSuccessListener { q ->
                q.documents.forEach {
                    it.toObject(Type::class.java)?.apply {
                        types.add(this)
                    }
                }

                val typesAdapter = TypesAdapter(types)
                recyclerViewTypes.apply {
                    adapter = typesAdapter
                    layoutManager = LinearLayoutManager(this@UserActivity)
                    setHasFixedSize(true)
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "get failed with ", it)
            }

        imageButtonEarth.setOnClickListener {
            val intent = Intent(this@UserActivity, LocationActivity::class.java)
            startActivityForResult(intent, RequestCodes.LOCATION)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCodes.LOCATION && resultCode == Activity.RESULT_OK) {
            data?.getParcelableExtra<LatLng>(IntentExtras.LAT_LNG)?.apply {
                Log.i(TAG, "latitude: $latitude longitude: $longitude")
            }
        }
    }

    private val seekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            textViewProgress.text = getString(R.string.num_meters, progress + 10)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

        override fun onStopTrackingTouch(seekBar: SeekBar?) {}
    }
}
