package com.example.arys

object IntentExtras {
    const val EMAIL = "email"
    const val PASSWORD = "password"
    const val LAT_LNG = "lat_lng"
}