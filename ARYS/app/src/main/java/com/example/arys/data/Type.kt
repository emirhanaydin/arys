package com.example.arys.data

data class Type(val ad: String, val aktif: Boolean) {
    @Suppress("unused")
    constructor() : this("", true)
}