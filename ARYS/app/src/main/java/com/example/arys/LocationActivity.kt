package com.example.arys

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_location.*

class LocationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        buttonCancel.setOnClickListener {
            it.isEnabled = false

            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        buttonSave.setOnClickListener {
            it.isEnabled = false

            val latLng = LatLng(
                editTextLatitude.text.toString().toDouble(),
                editTextLongitude.text.toString().toDouble()
            )
            intent.putExtra(IntentExtras.LAT_LNG, latLng)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
