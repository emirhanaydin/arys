# KOCAELİ ÜNİVERSİTESİ
# BİLGİSAYAR MÜHENDİSLİĞİ BÖLÜMÜ

# BLM306 YAZILIM LAB. II, 2018-2019

# PROJE 3

# PROJE TESLİM TARİHİ: 26/05/2019 Saat: 23:59

# AKILLI REKLAM YÖNETİM SİSTEMİ

Bu proje, Android uygulama ve bulut bilişim teknolojilerinin kullanılmasını amaçlamaktadır.

Projede akıllı reklam yönetim sistemi yapmanız istenmektedir. Android uygulama
geliştirmeniz beklenmektedir.

Android uygulamada bulunması beklenen isterler:

```
● Log in ekranı: Kullanıcı, kullanıcı adı ve şifresi ile giriş yapabilmeli, şifre
değiştirebilmeli veya kayıt olabilmeli.
● Kullanıcının konum bilgileri GPS üzerinden alınacaktır ve elle de giriş
yapılabilecektir.
● Kullanıcı, lokasyon bilgileri alınarak kullanıcının belirlediği mesafedeki mağazalardan
istediği kategoriye ait kampanyaları bildirim olarak ekranında görebilmelidir. Örneğin,
Ayşe İzmit Cumhuriyet Cad. gezerken karnının acıktığını fark eder. Daha sonra akıllı
reklam uygulamasını açarak, en fazla 100 metre uzaklıktaki yemek yerlerindeki
kampanyaları incelemek istemektedir. Uygulamaya 100 metre eşik (threshold) değeri
girerek yiyecek kategorisinde arama yapmaya başlar. Uygulama Ayşe’nin lokasyon
bilgisine ulaşır ve Ayşe’nin girmiş olduğu bilgilere göre varsa kampanyalar Ayşe’nin
ekranında gösterilir.
● Kullanıcı bilgileri telefonda tutulmayacak bulut üzerinden doğrulama (log-in)
yapılacaktır.
● Kullanıcı istediği durumlara göre arama işlemlerini kategoriye (yiyecek, giyim,
elektronik eşya, kozmetik vb.) ve mağaza adına göre yapabilecek.
● Android telefonu olmayan öğrenciler uygulamayı emulator üzerinde
gerçekleyebilirler.
```

Bulut Platformunda

```
● Veri tabanı tutulacak. Tablonuzda FirmaID, FirmaAdi, FirmaLokasyon,
KampanyaIcerik ve KampanyaSuresi bilgileri tutulacaktır.
● Bulut platformu üzerinde geliştireceğiniz API ile mobil uygulamanızın haberleşmesi
gerekmektedir.
● API’nizin ara yüzünden kampanya girişi yapılabilecek (Veri tabanına el ile kampanya
girişi yapılmayacaktır).
● Not: Bulut platformunda kısıtlamaya gidilmemiştir. IBM, Google ve Amazon gibi
cloud platformundan yararlanabilirsiniz.
```

## 1. Ödev Teslimi

```
● Proje sunum gününde rapor (hard copy) teslim edilmesi gerekmektedir.
● Rapor ieee formatında (önceki yıllarda verilen formatta) 4 sayfa, akış diyagramı veya yalancı
kod içeren, özet, giriş, yöntem, deneysel sonuçlar, sonuç ve kaynakça bölümünden
oluşmalıdır.
● Dersin takibi projenin teslimi dahil Google form sistemi üzerinden yapılacaktır. Belirtilen
tarihten sonra getirilen projeler kabul edilmeyecektir.
● Proje ile ilgili sorular Google form üzerinden Arş. Gör. Seda Kul ve Arş. Gör. Fulya
Akdeniz’e sorulabilir.
● Demo tarihleri daha sonra duyurulacaktır.
● Demo sırasında algoritma, geliştirdiğiniz kodun çeşitli kısımlarının ne amaçla yazıldığı ve
geliştirme ortamı hakkında sorular sorulabilir.
● Kullandığınız herhangi bir satır kodu açıklamanız istenebilir.
```
### KOPYA ÇEKTİĞİ YA DA KOPYA VERDİĞİ TESPİT EDİLEN ÖĞRENCİLER İÇİN KOPYA İŞLEMİ UYGULANACAKTIR.

## PROJE EN FAZLA İKİ KİŞİLİK GRUPLAR HALİNDE YAPILACAKTIR.


